class FlyingCrate extends Phaser.Physics.Arcade.Sprite
{
    constructor(scene, x, y)
    {
        super(scene, x + 32, y - 32, 'crate');
        this.scene = scene;
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);

        this.body.allowGravity = false;
        this.body.immovable = true;
        this.body.moves = false;

        this.scene.tweens.add({
            targets: this,
            y: y + 100,
            duration: 6000,
            ease: 'Sine.easeInOut',
            repeat: -1,
            yoyo: true
        });
    }
}