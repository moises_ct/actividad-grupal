class Player extends Phaser.Physics.Arcade.Sprite
{
    constructor(scene, x, y, bullets)
    {
        super(scene, x, y, 'player');

        this.bullets = bullets;
        this.scene = scene;
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        this.setBodySize(20, 55, false);
        this.setOffset(25, 12);
        
        this.lastFired = 0;

        // Cursor
        this.cursor = this.scene.input.keyboard.createCursorKeys();
       
        this.anims.create({
            key: 'walk',
            frames: this.scene.anims.generateFrameNames('sprites_jugador', { start: 1, end: 16, prefix: 'walk-' }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'run',
            frames: this.scene.anims.generateFrameNames('sprites_jugador', { start: 1, end: 8, prefix: 'run-' }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'runShoot',
            frames: this.scene.anims.generateFrameNames('sprites_jugador', { start: 1, end: 8, prefix: 'run-shoot-' }),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'idle',
            frames: this.scene.anims.generateFrameNames('sprites_jugador', { start: 1, end: 4, prefix: 'idle-' }),
            frameRate: 8,
            repeat: -1
        });
        this.anims.create({
            key: 'jump',
            frames: this.scene.anims.generateFrameNames('sprites_jugador', { start: 1, end: 4, prefix: 'jump-' }),
            frameRate: 8
        });

        this.shiftKey = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SHIFT);
        this.dKey = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    }

    update(time, delta)
    {
        if (this.cursor.left.isDown)
        {
            this.setSpeedAndFlip(-this.scene.data.playerData.walkSpeed, -this.scene.data.playerData.runSpeed, true);
        }
        else if (this.cursor.right.isDown)
        {
            this.setSpeedAndFlip(this.scene.data.playerData.walkSpeed, this.scene.data.playerData.runSpeed, false);
        }
        else
        {
            this.setVelocityX(0);
        }

        if (this.dKey.isDown && time > this.lastFired)
        {
            var bullet = this.bullets.get();

            if (bullet)
            {
                bullet.fire(this.x, this.y, this.flipX);
                this.lastFired = time + 200;
            }
        }

        if (this.cursor.space.isDown && this.body.onFloor()) 
        {
            this.setVelocityY(-this.scene.data.playerData.jumpImpulse);
        }

        if (!this.body.onFloor())
        {
            let absSpeedY = Math.abs(this.body.velocity.y);
            if (this.anims.getName() !== 'jump' && absSpeedY > this.scene.data.playerData.minSpeedForAnimJump)
            {
                this.play('jump');
            }
        }
        else if (this.body.velocity.x != 0)
        {
            let absSpeedX = Math.abs(this.body.velocity.x);
            let animationKey = (absSpeedX <= this.scene.data.playerData.walkSpeed) ? ('walk') : 
                (this.dKey.isDown ? ('runShoot') : ('run'));
            this.play(animationKey, true);
        }
        else
        {
            this.play('idle', true);
        }
    }

    setSpeedAndFlip(speedWalk, speedRun, flipX)
    {
        if (this.shiftKey.isDown)
        {
            this.setVelocityX(speedRun);
            this.setFlipX(flipX); 
        }
        else
        {
            this.setVelocityX(speedWalk);
            this.setFlipX(flipX); 
        }
    }
}