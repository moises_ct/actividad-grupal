class TutorialText extends Phaser.GameObjects.Text
{
    constructor(scene, x, y, text, config)
    {
        super(scene, x, y, text, config);

        this.isTutorialActive = false;
        this.alpha = 0;
        this.scene = scene;
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        this.body.allowGravity = false;

        this.scene.physics.add.overlap(this, this.scene.player, this.onShowTutorial, null, this);
    }

    onShowTutorial(sprite1, sprite2) 
    {
        if (!this.isTutorialActive)
        {
            this.scene.tweens.add({
                targets: sprite1,
                alpha: { value: 1, duration: 1000, ease: 'Sine.easeInOut' }
            });
            this.isTutorialActive = true;
        }
    }
}