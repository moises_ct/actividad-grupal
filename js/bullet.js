class Bullet extends Phaser.Physics.Arcade.Sprite
{
    constructor(scene)
    {
        super(scene, 0, 0, 'bullet');
        this.scene = scene;

        this.rotation = Math.PI * 0.5;
        this.speed = Phaser.Math.GetSpeed(this.scene.data.bulletData.speed, 1);

        this.playerPosX = 0;
        this.bulletDirection = 0;
    }

    fire(x, y, playerFlipX)
    {
        this.scene.audio["audio_shoot"].play();
        this.playerPosX = x;
        this.bulletDirection = (playerFlipX) ? (-1) : (1);
        this.setPosition(x + this.bulletDirection * 10, y - 4);

        this.setActive(true);
        this.setVisible(true);
    }

    update(time, delta)
    {
        this.x += this.bulletDirection * this.speed * delta;

        if ( (this.bulletDirection > 0 && this.x > this.playerPosX + this.scene.data.bulletData.distanceToMove) ||
        (this.bulletDirection < 0 && this.x < this.playerPosX - this.scene.data.bulletData.distanceToMove))
        {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}