class MainScene extends Phaser.Scene
{
    preload()
    {
        // Se usó esta librería para evitar las líneas negras del mapa
        // https://github.com/sporadic-labs/tile-extruder
        this.load.image('tiles', 'res/map/Tileset-extruded.png');
        this.load.tilemapTiledJSON('map', 'res/map/Map_v2.json');
        this.load.image('bg-1', 'res/background/nightbackgroundwithmoon.png');
        this.load.image('sea', 'res/background/sea.png');
        this.load.image('player', 'res/player_anim/idle-1.png');
        this.load.image('crate', 'res/sCrate.png');
        this.load.image('bullet', 'res/bullet.png');
        this.load.json('gamedata', 'js/data/gamedata.json');

        // https://gammafp.com/tool/atlas-packer/
        this.load.atlas('sprites_jugador', 'res/player_anim/player_animation.png', 'res/player_anim/player_animation_atlas.json');
        this.load.spritesheet('tilesSprites','res/map/Tileset-extruded.png', { frameWidth: 32, frameHeight: 32, margin: 1, spacing: 2 });

        // Cargando sonidos
        this.load.audio('music', 'res/sound/251461__joshuaempyre__arcade-music-loop.wav');
        this.load.audio('soundShoot', 'res/sound/344310__musiclegends__laser-shoot.wav');
    }

    create()
    {
        this.data = this.cache.json.get('gamedata');
        this.audio = {"audio_shoot": this.sound.add('soundShoot')};

        this.music = this.sound.add('music');
        this.music.volume = 0.3;
        this.music.loop = true;
        this.music.play();

        var bg_1 = this.add.tileSprite(windows.width * 0.5, windows.height * 0.5, windows.width, windows.height, 'bg-1');
        bg_1.setScrollFactor(0); // Esto reemplaza al anterior fixedToCamera

        var map = this.make.tilemap({ key: 'map' });
        var tiles = map.addTilesetImage('Plataformas', 'tiles');
        
        var layerDeath = map.createLayer('ZonaMuerta', tiles, 0, 0);
        layerDeath.setCollisionByExclusion(-1, true);
        map.createLayer('Fondo', tiles, 0, 0);
        var layer = map.createLayer('Suelo', tiles, 0, 0);

        // Colisiones para cada tile
        layer.setCollisionByExclusion(-1, true);

        // Creando balas para el personaje.
        this.bullets = this.add.group({
            classType: Bullet,
            maxSize: 10,
            runChildUpdate: true
        });

        // Necesitamos un player
        this.player = new Player(this, this.data.playerData.initX, this.data.playerData.initY, this.bullets);
        this.physics.add.collider(this.player, layer);

        // Detectamos la zona de muerte.
        this.physics.add.collider(this.player, layerDeath, this.onDeath, null, this);

        this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
        this.cameras.main.startFollow(this.player, false, 0.05, 1, -50);

        this.objetos = map.getObjectLayer('objetos')['objects'];
        this.setas = [];
        this.flyingCrates = [];
        for (var i = 0; i < this.objetos.length; ++i)
        {
            var obj = this.objetos[i];
            if (obj.gid == 115) // en mi caso la seta
            {
                var setaInstance = new Seta(this, obj.x, obj.y);
                this.setas.push(setaInstance);
                this.physics.add.overlap(setaInstance, this.player, this.spriteHit, null, this);
            }

            if (obj.gid == 99) // para crear las cajas voladoras
            {
                var crateInstance = new FlyingCrate(this, obj.x, obj.y);
                this.flyingCrates.push(crateInstance);
                this.physics.add.collider(crateInstance, this.player);
            }
        }

        this.score = 0;
        this.scoreText = this.add.text(16, 16, 'SETAS: ' + this.score, { 
            fontSize: '20px', 
            fill: '#000', 
            fontFamily: 'verdana, arial, sans-serif' 
          });
        this.scoreText.setScrollFactor(0);

        var configText = { 
            fontSize: '20px', 
            fill: '#000', 
            fontFamily: 'verdana, arial, sans-serif',
            wordWrap: { width: 225 } 
        };
        this.tutorial1 = new TutorialText(this, 420, 240, 'Para correr e ir más rápido, mantén presionado Shift', configText);
        this.tutorial2 = new TutorialText(this, 1100, 140, 'Para disparar, mantén presionado la tecla D', configText);
    }

    spriteHit(sprite1, sprite2) 
    {
        sprite1.destroy();
        this.score++;
        this.scoreText.text = 'SETAS: ' + this.score;
    }

    onDeath(sprite1, sprite2) 
    {
        sprite1.setPosition(this.data.playerData.initX, this.data.playerData.initY);
    }

    update(time, delta)
    {
        this.player.update(time, delta);
    }
}